// lấy thông tin sản phẩm từ Modal

export let layThongTin = () => {
  let id = document.getElementById("productID").value;
  let name = document.getElementById("productName").value;
  let price = document.getElementById("productPrice").value;
  let screen = document.getElementById("productScreen").value;
  let backCamera = document.getElementById("productBackCamera").value;
  let frontCamera = document.getElementById("productFrontCamera").value;
  let img = document.getElementById("productImg").value;
  let desc = document.getElementById("productDesc").value;
  let type = document.getElementById("productType").value;

  return {
    id,
    name,
    price,
    screen,
    backCamera,
    frontCamera,
    img,
    desc,
    type,
  };
};
