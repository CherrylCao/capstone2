var valid = true;
export let kiemTraDuLieuRong = (arrInput, arrNotiInput, product) => {
  var valid = true;
  for (var z = 0; z < arrInput.length; z++) {
    if (product[arrInput[z]] == "") {
      valid = valid && false;
      document.getElementById(arrNotiInput[z]).style.display = "inline-block";
      document.getElementById(arrNotiInput[z]).innerHTML =
        "Vui lòng nhập dữ liệu";
    } else {
      valid = valid && true;
      document.getElementById(arrNotiInput[z]).style.display = "none";
      document.getElementById(arrNotiInput[z]).innerHTML = "";
    }
  }
  return valid;
}
