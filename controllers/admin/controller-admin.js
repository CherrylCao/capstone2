// Hiển thị danh sách sản phẩm

export let renderProductList = (list) => {
  let contentHTML = "";
  list
    .reverse()
    .forEach(
      ({
        id,
        name,
        price,
        screen,
        backCamera,
        frontCamera,
        img,
        desc,
        type,
      }) => {
        let trString = `<tr>
                              <td>${id}</td>
                              <td>${name}</td>
                              <td>${price}</td>
                              <td>${screen}</td>
                              <td>${backCamera}</td>
                              <td>${frontCamera}</td>
                              <td>${type}</td>
                              <td><img class="w-100" src=${img}/></td>
                              <td>${desc}</td>
                              <td>
                                  <button class='btn-danger btn' onclick="deleteProduct(${id})">Xoá</button>
                                  <button class='btn-primary btn' onclick="editProduct(${id})">Sửa</button>
                              </td>
                          </tr>`;
        contentHTML += trString;
      }
    );
  document.getElementById("tbodyProduct").innerHTML = contentHTML;
};

export let onSuccess = (message) => {
  Swal.fire(message, "", "success");
};

export let showDataForm = (data) => {
  let { id, name, price, screen, backCamera, frontCamera, img, desc, type } =
    data;
  document.getElementById("productID").value = id;
  document.getElementById("productName").value = name;
  document.getElementById("productPrice").value = price;
  document.getElementById("productScreen").value = screen;
  document.getElementById("productBackCamera").value = backCamera;
  document.getElementById("productFrontCamera").value = frontCamera;
  document.getElementById("productImg").value = img;
  document.getElementById("productDesc").value = desc;
  document.getElementById("productType").value = type;
};
