import {
  onSuccess,
  renderProductList,
  showDataForm,
} from "./controller-admin.js";
import productServ from "../../service/service.js";
import { layThongTin } from "./addProduct-admin.js";
import { kiemTraDuLieuRong } from "./validation.js";
// Khai báo mảng cho validation

// var arrInput = [
//   id,
//   name,
//   price,
//   screen,
//   backCamera,
//   frontCamera,
//   img,
//   desc,
//   type,
// ];

// var arrNotiInput = [
//   "invalidID",
//   "invalidName",
//   "invalidGia",
//   "invalidScreen",
//   "invalidBackCamera",
//   "invalidFrontCamera",
//   "invalidImg",
//   "invalidDesc",
// ];

// Hiển thị danh sách sản phẩm

let fetchProductList = () => {
  productServ
    .getList()
    .then((res) => {
      console.log(res);
      renderProductList(res.data);
    })
    .catch((err) => {
      {
        console.log(err);
      }
    });
};
fetchProductList();

// Thêm sản phẩm

window.addProduct = () => {
  let data = layThongTin();
  // var valid = true;

  // valid = valid && kiemTraDuLieuRong(arrInput, arrNotiInput, product);
  // console.log(valid);

  // if (valid) {
  productServ
    .addProduct(data)
    .then((res) => {
      $("#productModal").modal("hide");
      onSuccess("Thêm thành công");
      fetchProductList();
    })
    .catch((err) => {
      console.log(err);
    });
  // }
};

// Xóa sản phẩm

window.deleteProduct = (id) => {
  productServ
    .deleteProduct(id)
    .then((res) => {
      console.log(res);
      onSuccess("Xóa thành công");
      fetchProductList();
    })
    .catch((err) => {
      console.log(err);
    });
};

// Sửa sản phẩm

window.editProduct = (id) => {
  document.getElementById("productID").readOnly = true;
  $("#productModal").modal("show");
  productServ
    .getDetail(id)
    .then((res) => {
      console.log(res);
      showDataForm(res.data);
    })
    .catch((err) => {
      console.log(err);
    });
};

// Nút cập nhật sản phẩm

window.updateProduct = () => {
  document.getElementById("productID").readOnly = false;
  let data = layThongTin();
  console.log(data);
  $("#productModal").modal("hide");
  onSuccess("Cập nhật thành công");
  productServ
    .updateProduct(data.id, data)
    .then((res) => {
      console.log(res);
      fetchProductList();
    })
    .catch((err) => {
      console.log(err);
    });
};
