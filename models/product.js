// Khởi tạo lớp đối tượng product

export default class Product {
  constructor(
    id,
    name,
    price,
    screen,
    backCamera,
    frontCamera,
    img,
    desc,
    type
  ) {
    this.id = id;
    this.name = name;
    this.price = price;
    this.screen = screen;
    this.backCamera = backCamera;
    this.frontCamera = frontCamera;
    this.img = img;
    this.desc = desc;
    this.type = type;
  }
}

export let showThongTin = (data) => {
  let { id, name, price, screen, backCamera, frontCamera, img, desc, type } =
    data;
  document.getElementById("productID").innerText = id;
  document.getElementById("productName").innerText = name;
  document.getElementById("productPrice").innerText = price;
  document.getElementById("productScreen").innerText = screen;
  document.getElementById("productBackCamera").innerText = backCamera;
  document.getElementById("productFrontCamera").innerText = frontCamera;
  document.getElementById("productImg").innerText = img;
  document.getElementById("productDesc").innerText = desc;
  document.getElementById("productType").src = type;
};
