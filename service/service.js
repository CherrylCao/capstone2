const BASE_URL = "https://64f168e00e1e60602d23c0f4.mockapi.io/CAPSTONE2";

// Lấy thông tin sản phẩm

let getList = () => {
  return axios({
    url: BASE_URL,
    method: "GET",
  });
};

// Thêm sản phẩm

let addProduct = (product) => {
  return axios({
    url: BASE_URL,
    method: "POST",
    data: product,
  });
};

// Xóa sản phẩm

let deleteProduct = (id) => {
  return axios({
    url: `${BASE_URL}/${id}`,
    method: "DELETE",
  });
};

// Lấy thông tin sản phẩm cần chỉnh sửa

let getDetail = (id) => {
  return axios({
    url: `${BASE_URL}/${id}`,
    method: "GET",
  });
};

let updateProduct = (id, newData) => {
  return axios({
    url: `${BASE_URL}/${id}`,
    method: "PUT",
    data: newData,
  });
};

// Gọi service

let productServ = {
  getList,
  addProduct,
  deleteProduct,
  getDetail,
  updateProduct,
  // kiemTraDuLieuRong,
};

export default productServ;
